Requirements:
sbt (tested with version 1.2.3 )
java 8 (tested with version 1.8.0_192)

all sbt commands to be run from base project directory


all output files will be output to the base project directory

Question One:
	run with 'sbt "runMain com.panaseer.QuestionOne"'
        outputs schema.txt

Question Two:
	run with 'sbt "runMain com.panaseer.QuestionTwo"'
        outputs number of records to stdout

Question Three
	run with 'sbt "runMain com.panaseer.QuestionThree"'
	outputs caseofficers.txt

Question Four
	run with 'sbt "runMain com.panaseer.QuestionFour"'
	inputs: prompted for number of agents by stdin. accepts integer value
	outputs agents.txt

Question Five
	run with 'sbt "runMain com.panaseer.QuestionFive"'
	outputs common_words.txt, with words from highest to lowest frequency

Question Six
	run with 'sbt "runMain com.panaseer.QuestionSix"'
	outputs average consultation length in days to stdout
