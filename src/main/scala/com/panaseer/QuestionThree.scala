package com.panaseer
import java.io.{File, PrintWriter}

import org.apache.spark.sql.Encoders

object QuestionThree {

  def main(args: Array[String]) {
    val (jsonData, spark) = Utils.getSpark()
    val caseOfficers = jsonData.select("CASEOFFICER")
      .where("CASEOFFICER != \"\"")
      .map(row => row.mkString)(Encoders.STRING)
      .collect()
      .toSet

    val pw = new PrintWriter(new File("caseofficers.txt" ))
    caseOfficers.foreach(pw.println)
    pw.close()
    spark.stop()
  }

}
