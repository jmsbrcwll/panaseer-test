package com.panaseer

import java.io.{File, PrintWriter}

import org.apache.spark.sql.Encoders
import org.apache.spark.sql.functions._

object QuestionFour {

  def main(args: Array[String]) {
    val(jsonData, spark) = Utils.getSpark()

    println("how many of the top N agents should be returned? ")

    val topN = readInt()

    val agents = jsonData.groupBy("AGENT")
      .count()
      .orderBy(desc("count"))
      .select("AGENT")
      .where("AGENT != \"\"")
      .limit(topN)
      .map(row => row.mkString)(Encoders.STRING)
      .filter(row => !row.isEmpty)
      .collect()

    val pw = new PrintWriter(new File("agents.txt" ))
    agents.foreach(pw.println)
    pw.close()
    spark.stop()
  }

}
