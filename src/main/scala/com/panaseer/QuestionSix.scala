package com.panaseer

import org.apache.spark.sql.{Row, SparkSession}

object QuestionSix {

  private var rollingAverage = 0L
  private var currentRowCount = 1

  def main(args: Array[String]) {
    val (jsonData, spark) = Utils.getSpark()
    jsonData.createOrReplaceTempView("planningApplications")

    spark.sql("select " +
      "  (UNIX_TIMESTAMP(PUBLICCONSULTATIONENDDATE, 'dd/MM/yyyy') -  UNIX_TIMESTAMP(PUBLICCONSULTATIONSTARTDATE, 'dd/MM/yyyy'))" +
      " as consultation_period FROM planningApplications")
      .foreach(r => addToRollingAverage(r))

    val averageDays = rollingAverage / 3600 / 24

    println(s"average days: $averageDays")

    spark.stop()
  }

  def addToRollingAverage(row: Row): Unit = {

    if (row.isNullAt(0)) {
      return
    }

    currentRowCount += 1
    if (currentRowCount > 1) {
      rollingAverage -= rollingAverage / currentRowCount
      rollingAverage += row.getLong(0) / currentRowCount
    }

    currentRowCount += 1
  }

}
