package com.panaseer

import java.io.{File, PrintWriter}

import org.apache.spark.sql.SparkSession

object QuestionOne {

    def main(args: Array[String]) {
      val (jsonData, spark) = Utils.getSpark()
      val pw = new PrintWriter(new File("schema.txt" ))
      pw.write(jsonData.schema.treeString)
      pw.close()
      spark.stop()
    }
}
