package com.panaseer

import java.io.{File, PrintWriter}

import org.apache.spark.sql.Row

import scala.collection.mutable

object QuestionFive {

  private val wordCountMap = mutable.HashMap[String, Int]()

  def main(args: Array[String]) {
    val (jsonData, spark) = Utils.getSpark()

    jsonData.select("CASETEXT").foreach(r => countWords(r))

    val sorted = wordCountMap.toList.sortBy(_._2).reverse

    val pw = new PrintWriter(new File("common_words.txt" ))
    pw.println("word,count")
    sorted.foreach(entry => pw.println(entry._1 + "," + entry._2))
    pw.close()
    spark.stop()
  }

  def countWords(row: Row) : Unit = {
    row.mkString.split("\\W+").foreach(addToCount)
  }

  //remove all punctuation and digits
  def cleanWord(word: String) : String = {
    word.replaceAll("""([\p{Punct}&&[^']]|[0-9])""", "").toLowerCase()
  }

  def addToCount(uncleanWord: String): Unit = {
    val word = cleanWord(uncleanWord)
    if (word.isEmpty) {
      return
    }

    val currentValue = wordCountMap.get(word)
    if (currentValue.nonEmpty) {
      wordCountMap.put(word, currentValue.get + 1)
    } else {
      wordCountMap.put(word, 1)
    }
  }
}
