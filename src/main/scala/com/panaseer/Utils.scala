package com.panaseer

import org.apache.spark.sql.{DataFrame, SparkSession}

object Utils {

  def getSpark(): (DataFrame, SparkSession) = {
    val dataFile = "planning-applications-weekly-list.json"
    val spark = SparkSession.builder
      .appName("Simple Application")
      .master("local[2]")
      .config("spark.executor.extraJavaOptions", "-Dlog4j.configuration=file://conf/log4j.properties")
      .getOrCreate()

    val jsonData = spark.read.json(dataFile)
    return (jsonData, spark)
  }

}
