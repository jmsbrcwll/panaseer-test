package com.panaseer

object QuestionTwo {

  def main(args: Array[String]) {
    val(jsonData, spark) = Utils.getSpark()
    val recordCount = jsonData.cache().count()
    println(s"there are $recordCount records in the dataset")
    spark.stop()
  }

}
